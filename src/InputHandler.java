import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode) {
        if (keycode == KeyCode.UP) {
            model.getThor().setMovingUp(true);
        }
        if (keycode == KeyCode.DOWN) {
            model.getThor().setMovingDown(true);
        }
        if (keycode == KeyCode.LEFT) {
            model.getThor().setMovingLeft(true);
        }
        if (keycode == KeyCode.RIGHT) {
            model.getThor().setMovingRight(true);
        }
        if (keycode == KeyCode.SPACE) {
            if (model.isThorAlive()) {
                model.createLightning();
            }
        }
    }

    public void onKeyReleased(KeyCode keycode) {
        if (keycode == KeyCode.UP) {
            model.getThor().setMovingUp(false);
        }
        if (keycode == KeyCode.DOWN) {
            model.getThor().setMovingDown(false);
        }
        if (keycode == KeyCode.LEFT) {
            model.getThor().setMovingLeft(false);
        }
        if (keycode == KeyCode.RIGHT) {
            model.getThor().setMovingRight(false);
        }
    }
}
