package model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {

    private Thor thor;
    private List<Loki> lokis = new LinkedList<>();
    private List<Lightning> lightnings = new LinkedList<>();

    //Größe des Fensters
    public static final double WIDTH = 1000;
    public static final double HEIGHT = 800;

    //Timerabhängige Variablen
    private long newWave = 1000; //in Millisekunden
    private long lightningRecharge = 100;

    private boolean thorAlive = true;
    private boolean gameWon = false;
    private int score = 0;
    private int i = 0;

    public Model() {
        this.thor = new Thor(80, 350); //Thor Startposition
    }

    private void createLokis() {
        Random random = new Random();
        for (int i = 0; i < 8; i++) {
            lokis.add(new Loki(1000, random.nextInt(720), 0.1f + random.nextFloat() * 0.2f));
        }
    }

    public void createLightning() {
        if (lightningRecharge < 0) {
            lightnings.add(new Lightning(getThor().getX(), this.getThor().getY() - 80, 0.5f));
            lightningRecharge = 200;
        }
    }

    public void update(long elapsedTime) {
        if (isThorAlive() && !isGameWon()) {
            thorMoves();
            timeElapsed(elapsedTime);
            cleaningUp(elapsedTime);
            collideThorWithLoki();
            collideLokiWithLightning();
        }
        if (!isThorAlive()) {
            thor.move(0, 2);
        }
    }

    private void thorMoves() {
        if (thorAlive) {
            if (thor.isMovingUp()) {
                thor.move(0, -5);
            }
            if (thor.isMovingDown()) {
                thor.move(0, 5);
            }
            if (thor.isMovingLeft()) {
                thor.move(-5, 0);
            }
            if (thor.isMovingRight()) {
                thor.move(5, 0);
            }
        }
    }

    private void timeElapsed(long elapsedTime) {
        for (Loki loki : lokis) {
            loki.update(elapsedTime);
        }

        newWave -= elapsedTime;
        if (newWave <= 0) {
            createLokis();
            newWave = 5000;
        }

        lightningRecharge -= elapsedTime;

    }

    private void cleaningUp(long elapsedTime) {
        while (i < lokis.size()) {
            if (lokis.get(i).getX() < 0) {
                lokis.remove(i);
            } else i++;
        }
        i = 0;
        for (Lightning lightning : lightnings) {
            lightning.update(elapsedTime);
        }
        while (i < lightnings.size()) {
            if (lightnings.get(i).getX() > 950) {
                lightnings.remove(i);
            } else i++;
        }
        i = 0;
    }

    private void collideThorWithLoki() {
        List<Integer> lokisToRemove = new LinkedList<>();
        for (Loki loki : lokis) {

            int dx = Math.abs(thor.getX() - loki.getX());
            int dy = Math.abs(thor.getY() - loki.getY());
            int w = thor.getW() / 2 + loki.getW() / 2 - 25;
            int h = thor.getH() / 2 + loki.getH() / 2;

            if (dx <= w && dy <= h) {
                lokisToRemove.add(lokis.indexOf(loki));
                int points = thor.getPoints();
                points -= 1;
                points = Math.max(points, 0);
                thor.setPoints(points);
                if (points > 0) {
                    System.out.println(points);
                    setThorAlive(true);
                } else {
                    System.out.println("Tot");
                    setThorAlive(false);
                }
            }
        }
        removeLoki(lokisToRemove);
    }

    private void collideLokiWithLightning() {
        List<Integer> lightningsToRemove = new LinkedList<>();
        List<Integer> lokisToRemove = new LinkedList<>();
        for (Lightning lightning : lightnings) {
            for (Loki loki : lokis) {

                int dx = Math.abs(loki.getX() - lightning.getX());
                int dy = Math.abs(loki.getY() - lightning.getY());
                int w = loki.getW() / 2 + lightning.getW() / 2;
                int h = loki.getH() / 2 + lightning.getH() / 2;

                if (dx <= w && dy <= h) {
                    lokisToRemove.add(lokis.indexOf(loki));
                    lightningsToRemove.add(lightnings.indexOf(lightning));
                    score += 1;
                    System.out.println(score);
                }
                if (score < 15) {
                    setGameWon(false);
                } else {
                    setGameWon(true);
                }
            }
        }
        removeLoki(lokisToRemove);
        removeLightning(lightningsToRemove);
    }

    private void removeLoki(List<Integer> lokisToRemove) {
        int j = 0;
        for (Integer loki : lokisToRemove) {
            lokis.remove(loki - j);
            j++;
        }
    }

    private void removeLightning(List<Integer> lightningsToRemove) {
        int k = 0;
        try {
            for (Integer lightning : lightningsToRemove) {
                lightnings.remove(lightning - k);
                k++;
            }
        } catch (Exception e) {
            System.out.print("");
        }
    }

    //Getter
    public Thor getThor() {
        return thor;
    }

    public List<Loki> getLokis() {
        return lokis;
    }

    public List<Lightning> getLightnings() {
        return lightnings;
    }

    public int getScore() {
        return score;
    }

    public boolean isThorAlive() {
        return thorAlive;
    }

    private boolean isGameWon() {
        return gameWon;
    }

    //Setter
    private void setThorAlive(boolean thorAlive) {
        this.thorAlive = thorAlive;
    }

    private void setGameWon(boolean gameWon) {
        this.gameWon = gameWon;
    }
}
