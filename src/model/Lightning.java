package model;

public class Lightning {

    private int x;
    private int y;
    private int h; //Height
    private int w; //Width
    private float speedX;

    public Lightning(int x, int y, float speedX) {
        this.x = x;
        this.y = y;

        this.speedX = speedX;
    }

    public void update(long elapsedTime) {
        this.x = Math.round(this.x + elapsedTime * this.speedX);
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return 20;
    }

    public int getW() {
        return 20;
    }
}
