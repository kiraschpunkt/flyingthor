package graphics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.scene.image.Image;

public class ImageCollection {

    private Image thoralive = new Image(new FileInputStream("src/graphics/files/thor-alive.png"));
    private Image thordead = new Image(new FileInputStream("src/graphics/files/thor-kaputt.png"));
    private Image lokiImage = new Image(new FileInputStream("src/graphics/files/loki-neu.png"));
    private Image blitzImage = new Image(new FileInputStream("src/graphics/files/blitz.png"));
    private Image landscape1 = new Image(new FileInputStream("src/graphics/files/landscape-1.png"));
    private Image landscape2 = new Image(new FileInputStream("src/graphics/files/landscape-2.png"));
    private Image landscape3 = new Image(new FileInputStream("src/graphics/files/landscape-3.png"));
    private Image landscape4 = new Image(new FileInputStream("src/graphics/files/landscape-4.png"));
    private Image landscape5 = new Image(new FileInputStream("src/graphics/files/landscape-5.png"));
    private Image thoragain = new Image(new FileInputStream("src/graphics/files/thor2.gif"));
    private Image plant1 = new Image(new FileInputStream("src/graphics/files/plant1.gif"));
    private Image plant2 = new Image(new FileInputStream("src/graphics/files/plant2.gif"));
    private Image jane = new Image(new FileInputStream("src/graphics/files/jane.jpg"));


    public ImageCollection() throws FileNotFoundException {
    }

    public javafx.scene.image.Image getThoralive() {
        return thoralive;
    }

    public javafx.scene.image.Image getThordead() {
        return thordead;
    }

    public javafx.scene.image.Image getLokiImage() {
        return lokiImage;
    }

    public javafx.scene.image.Image getBlitzImage() {
        return blitzImage;
    }

    public javafx.scene.image.Image getLandscape1() {
        return landscape1;
    }

    public javafx.scene.image.Image getLandscape2() {
        return landscape2;
    }

    public javafx.scene.image.Image getLandscape3() {
        return landscape3;
    }

    public javafx.scene.image.Image getLandscape4() {
        return landscape4;
    }

    public javafx.scene.image.Image getLandscape5() {
        return landscape5;
    }

    public Image getThoragain() {
        return thoragain;
    }

    public Image getPlant1() {
        return plant1;
    }

    public Image getPlant2() {
        return plant2;
    }

    public Image getJane() {
        return jane;
    }
}
