package graphics;

import model.Lightning;
import model.Loki;
import model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.io.FileNotFoundException;

public class Graphics {

    private GraphicsContext gc;
    private Model model;
    private ImageCollection image = new ImageCollection();

    public Graphics(GraphicsContext gc, Model model) throws FileNotFoundException {
        this.gc = gc;
        this.model = model;
    }

    public void draw() {
        if (model.isThorAlive()) {
            drawBackground();
            drawThor();
            drawLoki();
            drawLightning();
            drawScore();
        } else if (!model.isThorAlive()) {
            drawBackground();
            drawThor();
            drawLoki();
            drawLightning();
            drawScore();
            drawLoser();
        }
        if (model.getScore() >= 15) {
            drawWinner();
        }
    }

    public void drawBackground() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        if (model.getScore() < 3) {
            gc.drawImage(image.getLandscape1(), 0, 0, Model.WIDTH, Model.HEIGHT);
        } else if (model.getScore() < 6) {
            gc.drawImage(image.getLandscape2(), 0, 0, Model.WIDTH, Model.HEIGHT);
        } else if (model.getScore() < 9) {
            gc.drawImage(image.getLandscape3(), 0, 0, Model.WIDTH, Model.HEIGHT);
        } else if (model.getScore() < 12) {
            gc.drawImage(image.getLandscape4(), 0, 0, Model.WIDTH, Model.HEIGHT);
        } else if (model.getScore() < 15) {
            gc.drawImage(image.getLandscape5(), 0, 0, Model.WIDTH, Model.HEIGHT);
//        } else {
//            gc.drawImage(image.getLoser(), 0, 0, Model.WIDTH, Model.HEIGHT);
        }
    }

    public void drawThor() {
        if (model.isThorAlive()) {
            gc.drawImage(image.getThoralive(),
                    model.getThor().getX() - model.getThor().getW(),
                    model.getThor().getY() - model.getThor().getH(), 120, 120);
        } else {
            gc.drawImage(image.getThordead(),
                    model.getThor().getX() - model.getThor().getW(),
                    model.getThor().getY() - model.getThor().getH(), 120, 120);
        }
        // gc.setFill(Color.BLACK);
        // gc.fillRect(model.getThor().getX() - model.getThor().getW(),
        // model.getThor().getY() - model.getThor().getH() ,120,110);
    }

    public void drawLoki() {
        for (Loki loki : this.model.getLokis()) {
            // gc.fillRect( loki.getX() - loki.getW() / 2, loki.getY() - loki.getH() / 2, loki.getW(), loki.getH());
            gc.drawImage(image.getLokiImage(),
                    loki.getX() - loki.getW() + 30,
                    loki.getY() - loki.getH() + 10, 90, 90);
        }
    }

    public void drawLightning() {
        for (Lightning lightning : model.getLightnings()) {
            gc.drawImage(image.getBlitzImage(), lightning.getX(), lightning.getY(), 60, 30);
        }
    }

    public void drawScore() {

        gc.setFont(Font.font("Gabriola", 30));
        gc.setFill(Color.GOLD);
        gc.setStroke(Color.DARKORANGE);
        gc.setLineWidth(1);
        gc.strokeText("Lives left  " + model.getThor().getPoints(), 20, 40);
        gc.strokeText("Score  " + model.getScore(), Model.WIDTH - 100, 40);
        gc.fillText("Lives left  " + model.getThor().getPoints(), 20, 40);
        gc.fillText("Score  " + model.getScore(), Model.WIDTH - 100, 40);
    }

    public void drawLoser() {
        //gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
//        gc.drawImage(image.getLoser(), 500, 400, 200, 200);
        gc.setFont(Font.font("Gabriola", 150));
        gc.setStroke(Color.DARKORANGE);
        gc.setLineWidth(5);
        gc.strokeText("YOU LOOSE!", 180, 400);
        gc.setFill(Color.GOLD);
        gc.fillText("YOU LOOSE!", 180, 400);
    }

    public void drawWinner() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.setFill(Color.CADETBLUE);
        gc.fillRect(0, 450, Model.WIDTH, Model.HEIGHT / 2);
        gc.setFill(Color.DARKBLUE);
        gc.fillRect(0, 0, Model.WIDTH, 550);
        gc.drawImage(image.getThoragain(), 320, 320, 300, 300);
        gc.drawImage(image.getPlant1(), 100, 400, 150, 170);
        gc.drawImage(image.getPlant2(), 750, 420, 150, 150);
        gc.drawImage(image.getJane(), 625, 280, 100, 125);
        gc.setFont(Font.font("Gabriola", 150));
        gc.setStroke(Color.DARKORANGE);
        gc.setLineWidth(5);
        gc.strokeText("YOU WON!", 200, 200);
        gc.setFill(Color.GOLD);
        gc.fillText("YOU WON!", 200, 200);

    }
}
